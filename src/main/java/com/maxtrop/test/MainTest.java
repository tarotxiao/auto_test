package com.maxtrop.test;

import com.maxtrop.test.scene.BaseCase;
import com.maxtrop.test.scene.status.GetDevice;
import com.maxtrop.test.scene.strategy.CreateAppStrategy;
import com.maxtrop.test.scene.strategy.GetProcess;
import com.maxtrop.test.scene.strategy.OperateAppStrategy;
import com.maxtrop.test.scene.system.Login;
import com.maxtrop.test.scene.system.Refresh;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by hanxiao on 2016/12/12.
 */
public class MainTest {

    static {
        //加载chrome驱动
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
    }

    public static void main(String[] args) throws InterruptedException {
        if(args[0]!=null){
            System.setProperty("webdriver.chrome.driver", args[0]);
        }
        WebDriver dr = null;
        try {
            //初始化
            dr = new ChromeDriver();
            BaseCase.setDR(dr);
            //登录case
            Login login = new Login();
            login.login("test3", "123456");
            //创建一个推送策略
            CreateAppStrategy cs = new CreateAppStrategy();
            cs.appStrategyPage();
            cs.prepare("自动化测试安装");
            cs.chooseGroup();
            cs.chooseAppAndVersion(new boolean[]{false, true});
            cs.create();
            //应用推送策略
            OperateAppStrategy push = new OperateAppStrategy();
            push.push();

            //刷新
            Refresh.refresh();

            //查询结果
            GetProcess gp = new GetProcess();
            while (!gp.getProcess()) {
                System.out.println("等待结果");
                Thread.sleep(5000);
            }
            //查看设备详情
            GetDevice gd = new GetDevice();
            Thread.sleep(3000);
            gd.devicePage();
            gd.getApp("hanxiaotest");

            Thread.sleep(5000);
            //登出case
            //Logout.logout();
            System.out.println("测试结束");
        } finally {
            if (dr != null)
                dr.quit();
        }
    }

}
