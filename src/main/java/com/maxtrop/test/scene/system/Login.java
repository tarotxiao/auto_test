package com.maxtrop.test.scene.system;

import com.maxtrop.test.Tools;
import com.maxtrop.test.scene.BaseCase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by hanxiao on 2016/12/12.
 */
public class Login extends BaseCase {

    {
        dr.get("http://10.9.8.105:9000/index.html");
    }

    public void login(String userName,String password) throws InterruptedException {
        Thread.sleep(2000);
        dr.findElement(By.id("name")).sendKeys(userName);
        Thread.sleep(2000);
        dr.findElement(By.id("password")).sendKeys(password);
        Thread.sleep(2000);
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/div[2]/button")).click();
        dr.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement webElement = Tools.fluentWait(By.xpath("//*[@id=\"content\"]/div/div/div[1]/div[2]/div/div[1]/span"), dr);
        String text = webElement.getText();
        if (text.contains("test")) {
            System.out.println("登录测试成功！");
        } else {
            System.out.println("登录测试失败！");
        }
    }


}
