package com.maxtrop.test.scene.system;

import com.maxtrop.test.scene.BaseCase;

/**
 * Created by hanxiao on 2016/12/13.
 */
public class Refresh extends BaseCase {

    public static void refresh() {
        dr.navigate().refresh();
    }

}
