package com.maxtrop.test.scene.system;

import com.maxtrop.test.scene.BaseCase;
import org.openqa.selenium.By;

/**
 * Created by hanxiao on 2016/12/12.
 */
public class Logout extends BaseCase {

    public static void logout() throws InterruptedException {
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[1]/div[2]/div/div[1]/button")).click();
        System.out.println("登出成功！");
    }

}
