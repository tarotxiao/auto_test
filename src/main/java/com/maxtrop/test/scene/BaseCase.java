package com.maxtrop.test.scene;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by hanxiao on 2016/12/12.
 */
public abstract class BaseCase {

    protected static WebDriver dr;

    public static void setDR(WebDriver driver) {
        BaseCase.dr = driver;
        if (dr instanceof ChromeDriver) {
            //dr.manage().window().maximize();
        }
    }
}
