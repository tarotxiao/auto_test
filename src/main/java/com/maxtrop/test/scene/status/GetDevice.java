package com.maxtrop.test.scene.status;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

/**
 * Created by hanxiao on 2016/12/13.
 */
public class GetDevice extends DevcieEnter{

    public void getApp(String deviceId) throws InterruptedException {
        //填写设备编号
        dr.findElement(By.cssSelector("input")).sendKeys(deviceId);
        Thread.sleep(1000);
        //点击搜索
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/section/div[1]/div/div[2]/button")).click();
        Thread.sleep(1000);
        //滚动到底部
        JavascriptExecutor jse = (JavascriptExecutor)dr;
        jse.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
        Thread.sleep(1000);
    }
}
