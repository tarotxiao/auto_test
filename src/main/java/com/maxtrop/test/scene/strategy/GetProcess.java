package com.maxtrop.test.scene.strategy;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

/**
 * Created by hanxiao on 2016/12/13.
 */
public class GetProcess extends AppStrategyEnter{

    public boolean getProcess() throws InterruptedException {
        //点击查看进度
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/section/div[1]/div/div[2]/div[2]/div[1]/div/button[2]")).click();
        Thread.sleep(1000);
        String text = dr.findElement(By.xpath("/html/body/div[8]/div/div[1]/div/div/div/div/div[2]/div/div[2]/table/tbody/tr/td[2]")).getText();
        int except = StringUtils.countMatches(text, ":");
        int result = StringUtils.countMatches(text, "已更新");
        Thread.sleep(1000);
        //关闭
        dr.findElement(By.xpath("/html/body/div[8]/div/div[1]/div/div/div/div/div[3]/button")).click();
        return except==result;
    }

}
