package com.maxtrop.test.scene.strategy;

import com.maxtrop.test.scene.BaseCase;
import org.openqa.selenium.By;

/**
 * Created by hanxiao on 2016/12/12.
 */
public abstract class AppStrategyEnter extends BaseCase {

    public void appStrategyPage() throws InterruptedException {
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/nav/div/div[3]/span/div")).click();
        Thread.sleep(1000);
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/nav/div/div[3]/div/div[2]/span")).click();
        Thread.sleep(1000);
    }

}
