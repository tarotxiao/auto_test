package com.maxtrop.test.scene.strategy;

import org.openqa.selenium.By;

/**
 * Created by hanxiao on 2016/12/12.
 */
public class CreateAppStrategy extends AppStrategyEnter {

    public void prepare(String name) throws InterruptedException {
        //点击新建
        dr.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[2]/section/div[1]/div/div[2]/div[1]/button")).click();
        Thread.sleep(1000);
        //填写策略名
        dr.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys(name);
        Thread.sleep(1000);
    }

    public void chooseGroup()throws InterruptedException {
        //选组
        dr.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/div/div/div/div[3]/div")).click();
        Thread.sleep(1000);
        dr.findElement(By.xpath("/html/body/div[67]/div/div/div/div[2]")).click();
        Thread.sleep(1000);
    }

    public void create()throws InterruptedException {
        //确认
        dr.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/div/div/div/div[6]/button[1]")).click();
        Thread.sleep(1000);
    }

    public void chooseAppAndVersion(boolean[] version) throws InterruptedException {
        for(int i=0;i<version.length;i++){
            //选择app
            //点击+按钮
            dr.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/div/div/div/div[4]/div/div["+(i+1)+"]")).click();
            Thread.sleep(1000);
            //选择app
            dr.findElement(By.xpath("/html/body/div[65]/div/div[1]/div/div/div/div/div[2]/div[1]/div")).click();
            Thread.sleep(1000);
            //下拉版本列表
            dr.findElement(By.xpath("/html/body/div[65]/div/div[1]/div/div/div/div/div[4]/div")).click();
            Thread.sleep(1000);
            //点击版本
            if(version[i]){
                //低版本
                dr.findElement(By.xpath("/html/body/div[67]/div/div/div/div[2]")).click();
            }else {
                //高版本
                dr.findElement(By.xpath("/html/body/div[67]/div/div/div/div[3]")).click();
            }
            Thread.sleep(1000);
            if(i==0){
                //勾选启动项
                dr.findElement(By.xpath("//*[@id=\"bootstrap\"]")).click();
                Thread.sleep(1000);
            }
            //确认单个app
            dr.findElement(By.xpath("/html/body/div[65]/div/div[1]/div/div/div/div/div[6]/button[1]")).click();
            Thread.sleep(1000);
        }
        System.out.println("创建策略成功");
    }

}
