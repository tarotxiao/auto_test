package com.maxtrop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

/**
 * Created by hanxiao on 2016/12/12.
 */
public class Tools {

    public static WebElement fluentWait(final By locator,WebDriver dr) {
        Wait<WebDriver> wait = new FluentWait<>(dr)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(driver -> driver.findElement(locator));

        return  foo;
    };

}
